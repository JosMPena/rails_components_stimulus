# README

This Project is intendend to try the 'components' architecture with Rails using the gem Komposable with Stimulus framework

Topics covered on this project:

* Wire up the rails front-end elements as components

* Use the Komponents gem to manage all components

* Use Stimulus as Front End "framework"
