class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # Read templates from frontent folder
  prepend_view_path Rails.root.join('frontend')
end
